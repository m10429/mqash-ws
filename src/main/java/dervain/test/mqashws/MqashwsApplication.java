package dervain.test.mqashws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MqashwsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MqashwsApplication.class, args);
	}

}
