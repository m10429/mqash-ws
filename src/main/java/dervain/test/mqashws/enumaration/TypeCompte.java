package dervain.test.mqashws.enumaration;

import lombok.Getter;

public enum TypeCompte {

    COMPTE_EPARGNE("Compte épargne"),
    COMPTE_COURANT("Compte courant");



    @Getter
    private final String value;

    TypeCompte(String value) {
        this.value = value;
    }
}
