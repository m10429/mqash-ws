package dervain.test.mqashws.enumaration;

public enum TypeTransaction {
    VIREMENT_INTERNE, VERSEMENT, RETRAIT
}
