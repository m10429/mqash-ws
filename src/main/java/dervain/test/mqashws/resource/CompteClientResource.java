package dervain.test.mqashws.resource;

import dervain.test.mqashws.models.Client;
import dervain.test.mqashws.models.CompteClient;
import dervain.test.mqashws.service.CompteClientService;
import dervain.test.mqashws.service.dto.CompteClientDto;
import dervain.test.mqashws.utilitaire.ResponseUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/compte-client")
public class CompteClientResource {

    private final CompteClientService compteClientService;

    @PostMapping("/add")
    public ResponseEntity<CompteClient> createCpteClient(@RequestBody @Valid CompteClientDto requestDto)
    {
        log.info("Request to save a customer");
        var result = compteClientService.save(requestDto);
        return ResponseEntity.created(URI.create("/mqash-ws/compte-client/get/"+result.getId())).body(result);
    }

    @GetMapping("/get/{numeroCompte}")
    public ResponseEntity<CompteClient> getOneClient(@PathVariable("numeroCompte") String id)
    {
        log.info("Request to get a customer account");
        var result = compteClientService.findByNumeroCompte(id);
        return ResponseUtil.wrapOrNotFound(result);
    }

    @GetMapping("/list-by-customerId/{id}")
    public ResponseEntity<List<CompteClient>> listCompteClientByIdCLient(@PathVariable("id") Long idClient)
    {
        log.info("Request to get a customer");
        var result = compteClientService.listByClientId(idClient);
        return ResponseEntity.ok(result);
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteClient(@PathVariable Long id) {
        log.info("Request to get a customer");
        compteClientService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
