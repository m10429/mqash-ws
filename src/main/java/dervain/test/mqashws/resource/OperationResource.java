package dervain.test.mqashws.resource;

import dervain.test.mqashws.models.CompteClient;
import dervain.test.mqashws.models.Operation;
import dervain.test.mqashws.models.Response;
import dervain.test.mqashws.service.OperationService;
import dervain.test.mqashws.service.dto.CompteClientDto;
import dervain.test.mqashws.service.dto.TransactionDto;
import dervain.test.mqashws.utilitaire.ResponseUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping("/operation")
@RequiredArgsConstructor
public class OperationResource {
    private final OperationService operationService;

    @PostMapping("/transaction")
    public ResponseEntity<Response> createTransaction(@RequestBody @Valid TransactionDto requestDto) {
        log.info("Request to save a customer");
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(LocalDateTime.now())
                        .data(Map.of("Transaction", operationService.saveTransaction(requestDto)))
                        .message("transaction effectuée")
                        .status(HttpStatus.OK)
                        .statusCode(HttpStatus.OK.value()).build()
        );
    }


}
