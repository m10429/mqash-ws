package dervain.test.mqashws.resource;

import dervain.test.mqashws.models.Client;
import dervain.test.mqashws.service.ClientService;
import dervain.test.mqashws.utilitaire.ResponseUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/client")
@RequiredArgsConstructor
@Slf4j
public class ClientResource {

    private final ClientService cLientService;

    @PostMapping("/add")
    public ResponseEntity<Client> createClient(@RequestBody @Valid Client client)
    {
        log.info("Request to save a customer");
        var result = cLientService.save(client);
        return ResponseEntity.created(URI.create("/mqash-ws/client/get/"+result.getId())).body(result);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<Client> getOneClient(@PathVariable("id") Long id)
    {
        log.info("Request to get a customer");
        var result = cLientService.findClientById(id);
        return ResponseUtil.wrapOrNotFound(result);
    }

    @GetMapping("/list")
    public ResponseEntity<List<Client>> listClient()
    {
        log.info("Request to get a customer");
        var result = cLientService.findAll();
        return ResponseEntity.ok(result);
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteClient(@PathVariable Long id) {
        log.info("Request to get a customer");
        cLientService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
