package dervain.test.mqashws.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import dervain.test.mqashws.enumaration.TypeTransaction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Operation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private LocalDateTime dateOperation;

    @Enumerated(EnumType.STRING)
    private TypeTransaction typeTransaction;
    private int montantOperation;

    @ManyToOne
    private CompteClient compteClientCible;

    @ManyToOne
    private CompteClient compteClient;


}
