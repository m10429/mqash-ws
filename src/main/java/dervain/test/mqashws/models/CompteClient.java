package dervain.test.mqashws.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import dervain.test.mqashws.enumaration.TypeCompte;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.LocalDateTime;


@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompteClient implements Serializable {
    @Id
    private Long id;

    @Column(unique = true)
    @NotEmpty(message = "le numero du compte est obligatoire")
    private String numeroCompte;
    private String libelle;
    @Enumerated(EnumType.STRING)
    private TypeCompte typeCompte;

    private LocalDateTime dateOuverture;
    private LocalDateTime dateFermeture;

    private int compteSoldeReel;
    private int compteSoldeBanque;

    private boolean cloture = false;

    @ManyToOne
    private Client client;

}
