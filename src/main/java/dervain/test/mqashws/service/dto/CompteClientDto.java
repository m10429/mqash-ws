package dervain.test.mqashws.service.dto;

import dervain.test.mqashws.enumaration.TypeCompte;
import dervain.test.mqashws.models.Client;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CompteClientDto implements Serializable {

    private Long id;

    private String numeroCompte;
    private String libelle;
    @Enumerated(EnumType.STRING)
    private TypeCompte typeCompte;

    private LocalDateTime dateOuverture;
    private LocalDateTime dateFermeture;
    private int compteSoldeReel;
    private int compteSoldeBanque;
    private boolean cloture = false;
    private Long clientId;
}
