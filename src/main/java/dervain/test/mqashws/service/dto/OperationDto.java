package dervain.test.mqashws.service.dto;

import dervain.test.mqashws.enumaration.TypeTransaction;
import dervain.test.mqashws.models.CompteClient;

import javax.persistence.*;
import java.time.LocalDateTime;

public class OperationDto {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private LocalDateTime dateOperation;

    @Enumerated(EnumType.STRING)
    private TypeTransaction typeTransaction;
    private int montantOperation;

    @ManyToOne
    private CompteClient compteClient;

}
