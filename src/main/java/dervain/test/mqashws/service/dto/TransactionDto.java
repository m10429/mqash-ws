package dervain.test.mqashws.service.dto;

import dervain.test.mqashws.enumaration.TypeTransaction;
import dervain.test.mqashws.models.Operation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionDto {


    private Long id;

    private LocalDateTime dateOperation;

    @Enumerated(EnumType.STRING)
    private TypeTransaction typeTransaction;
    private int montantOperation;

    private String numeroCompteAuteur;
    private String numeroCompteCible;
}
