package dervain.test.mqashws.service;

import dervain.test.mqashws.models.Client;

import java.util.List;
import java.util.Optional;

public interface ClientService {

    Optional<Client> findClientById(Long idClient);
    Client save(Client client);
    List<Client> findAll();
    Boolean delete(Long id);
}
