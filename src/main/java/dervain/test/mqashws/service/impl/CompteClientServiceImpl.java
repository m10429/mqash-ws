package dervain.test.mqashws.service.impl;

import dervain.test.mqashws.models.Client;
import dervain.test.mqashws.models.CompteClient;
import dervain.test.mqashws.repository.ClientRepository;
import dervain.test.mqashws.repository.CompteClientRepository;
import dervain.test.mqashws.service.ClientService;
import dervain.test.mqashws.service.CompteClientService;
import dervain.test.mqashws.service.dto.CompteClientDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
@Slf4j
@RequiredArgsConstructor
public class CompteClientServiceImpl implements CompteClientService {

    private final CompteClientRepository compteClientRepository;

    @Override
    public CompteClient save(CompteClientDto compteClientDto) {
        var numeroCompte = RandomStringUtils.randomAlphanumeric(15);
        var compteClient = CompteClient.builder()
                .compteSoldeBanque(compteClientDto.getCompteSoldeBanque())
                .compteSoldeReel(compteClientDto.getCompteSoldeReel())
                .dateOuverture(compteClientDto.getDateOuverture())
                .libelle(compteClientDto.getLibelle())
                .numeroCompte(numeroCompte)
                .build();
        return compteClientRepository.save(compteClient);
    }

    @Override
    public List<CompteClient> listByClientId(Long idClient) {
        return compteClientRepository.findAllByClientId(idClient);
    }

    @Override
    public Optional<CompteClient> findByNumeroCompte(String numeroCompte) {
        return compteClientRepository.findByNumeroCompte(numeroCompte);
    }

    @Override
    public void delete(Long id) {

    }
}
