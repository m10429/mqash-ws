package dervain.test.mqashws.service.impl;

import dervain.test.mqashws.enumaration.TypeTransaction;
import dervain.test.mqashws.exception.ApiException;
import dervain.test.mqashws.models.Operation;
import dervain.test.mqashws.repository.CompteClientRepository;
import dervain.test.mqashws.repository.OperationRepository;
import dervain.test.mqashws.service.CompteClientService;
import dervain.test.mqashws.service.OperationService;
import dervain.test.mqashws.service.dto.TransactionDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static java.util.Objects.isNull;

@Service
@RequiredArgsConstructor
@Slf4j
public class OperationServiceImpl implements OperationService {

    private final OperationRepository operationRepository;
    private final CompteClientService compteClientService;
    private final CompteClientRepository compteClientRepository;

    @Override
    public Operation saveTransaction(TransactionDto requestDto) {
        log.info("requête pour effectuer une transation");
        int montant = 0;
        Operation operation;
        var compteAuteurTransaction = compteClientService.findByNumeroCompte(requestDto.getNumeroCompteAuteur()).orElseThrow(
                () -> new ApiException("compte inexistant", 404)
        );
        var compteClientCible = compteClientService.findByNumeroCompte(requestDto.getNumeroCompteCible()).orElseThrow(
                () -> new ApiException("compte inexistant", 404)
        );
        if (!isNull(requestDto.getTypeTransaction()) && requestDto.getTypeTransaction().equals(TypeTransaction.VIREMENT_INTERNE)) {

            if (compteAuteurTransaction.getCompteSoldeReel() > requestDto.getMontantOperation()) {
                operation = Operation.builder()
                        .compteClient(compteAuteurTransaction)
                        .compteClientCible(compteClientCible)
                        .dateOperation(LocalDateTime.now())
                        .montantOperation(requestDto.getMontantOperation())
                        .typeTransaction(TypeTransaction.VIREMENT_INTERNE)
                        .build();
                compteAuteurTransaction.setCompteSoldeReel( compteAuteurTransaction.getCompteSoldeReel() - requestDto.getMontantOperation());
                compteClientCible.setCompteSoldeReel( compteClientCible.getCompteSoldeReel() + requestDto.getMontantOperation());
                compteClientRepository.saveAll(List.of(compteAuteurTransaction, compteClientCible));
            } else throw new ApiException("Impossible d'effectuer cette operation; Solde insufisant", 400);

        } else if (!isNull(requestDto.getTypeTransaction()) && requestDto.getTypeTransaction().equals(TypeTransaction.RETRAIT)) {
            if (compteAuteurTransaction.getCompteSoldeReel() > requestDto.getMontantOperation()) {
                operation = Operation.builder()
                        .compteClient(compteAuteurTransaction)
                        .dateOperation(LocalDateTime.now())
                        .montantOperation(requestDto.getMontantOperation())
                        .typeTransaction(TypeTransaction.RETRAIT)
                        .build();
                compteAuteurTransaction.setCompteSoldeReel( compteAuteurTransaction.getCompteSoldeReel() - requestDto.getMontantOperation());
                compteClientRepository.saveAll(List.of(compteAuteurTransaction));
            } else throw new ApiException("Impossible d'effectuer cette operation; Solde insufisant", 400);
        } else  {
            operation = Operation.builder()
                    .compteClient(compteAuteurTransaction)
                    .dateOperation(LocalDateTime.now())
                    .montantOperation(requestDto.getMontantOperation())
                    .typeTransaction(TypeTransaction.VERSEMENT)
                    .build();
            compteAuteurTransaction.setCompteSoldeReel( compteAuteurTransaction.getCompteSoldeReel() + requestDto.getMontantOperation());
            compteClientRepository.saveAll(List.of(compteAuteurTransaction));

        }

        return operationRepository.save(operation);
    }
}
