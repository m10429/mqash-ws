package dervain.test.mqashws.service.impl;

import dervain.test.mqashws.models.Client;
import dervain.test.mqashws.repository.ClientRepository;
import dervain.test.mqashws.service.ClientService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    @Override
    public Optional<Client> findClientById(Long idClient) {
        return clientRepository.findById(idClient);
    }

    @Override
    public Client save(Client client) {
        return clientRepository.save(client);
    }

    @Override
    public List<Client> findAll() {
        return clientRepository.findAll();
    }

    @Override
    public Boolean delete(Long id) {
        return null;
    }
}
