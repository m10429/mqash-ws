package dervain.test.mqashws.service;

import dervain.test.mqashws.models.CompteClient;
import dervain.test.mqashws.service.dto.CompteClientDto;

import java.util.List;
import java.util.Optional;

public interface CompteClientService {

    CompteClient save(CompteClientDto compteClientDto);
    List<CompteClient> listByClientId(Long idClient);
    Optional<CompteClient> findByNumeroCompte(String numeroCompte);
    void delete(Long id);
}
