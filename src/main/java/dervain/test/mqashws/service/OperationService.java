package dervain.test.mqashws.service;

import dervain.test.mqashws.models.Operation;
import dervain.test.mqashws.service.dto.OperationDto;
import dervain.test.mqashws.service.dto.TransactionDto;

public interface OperationService {
    Operation saveTransaction(TransactionDto requestDto);
}
