package dervain.test.mqashws.repository;

import ch.qos.logback.core.net.server.Client;
import dervain.test.mqashws.models.CompteClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CompteClientRepository extends JpaRepository<CompteClient, Long> {

    List<CompteClient> findAllByClientId(Long idClient);


    Optional<CompteClient> findByNumeroCompte(String numeroCompte);
}
